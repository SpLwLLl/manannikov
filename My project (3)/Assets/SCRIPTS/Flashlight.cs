using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flashlight : MonoBehaviour
{
    public GameObject fonar;
    public bool On;
    // Update is called once per frame
    private void Start()
    {
        fonar.SetActive(false);
        On = true;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F) & On == false)
        {
            fonar.SetActive(false);
            On = true;
        }

        else if (Input.GetKeyDown(KeyCode.F) & On == true)
        {
            fonar.SetActive(true);
            On = false;
        }
    }
}