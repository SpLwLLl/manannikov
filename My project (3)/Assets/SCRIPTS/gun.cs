using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR;
using Valve.VR.InteractionSystem;


public class gun : MonoBehaviour
{
    public SteamVR_Action_Boolean fireAction;
    public GameObject bullet;
    public Transform barrePivot;
    public GameObject muzzleFlash;
    public float ShootingSpeed = 3;


    private Interactable interactable;

    void Start()
    {
        muzzleFlash.SetActive(false);
        interactable = GetComponent<Interactable>();
    }


    void Update()
    {
        if (interactable.attachedToHand != null)
        {
            SteamVR_Input_Sources source = interactable.attachedToHand.handType;
            if (fireAction[source].stateDown)
            {
                Fire();
            }
        }
    }
    public void Fire()
    {
        Rigidbody BulletRigidbody = Instantiate(bullet, barrePivot.position, barrePivot.rotation).GetComponent<Rigidbody>();
        BulletRigidbody.velocity = barrePivot.up * ShootingSpeed;
        muzzleFlash.SetActive(false);
    }
}
